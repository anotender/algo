//
// Created by matnow on 10/11/18.
//

#ifndef LAB1_EDGE_H
#define LAB1_EDGE_H


#include "Vertex.h"

class Edge {

public:
    Edge(Vertex &from, Vertex &to, int weight);

    Vertex &getFrom() const;

    Vertex &getTo() const;

    int getWeight() const;

private:
    Vertex &from;
    Vertex &to;
    int weight;
};


#endif //LAB1_EDGE_H
