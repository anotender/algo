//
// Created by matnow on 10/14/18.
//

#ifndef LAB1_MATRIXSHORTESTPATHALGORITHM_H
#define LAB1_MATRIXSHORTESTPATHALGORITHM_H


#include "ShortestPathAlgorithm.h"

class MatrixShortestPathAlgorithm : public ShortestPathAlgorithm {
public:
    void compute(int *distanceArray, int *predecessorArray) override;

    MatrixShortestPathAlgorithm(int numberOfVertices, char *fileName);

private:
    int numberOfVertices;
    int **incidenceMatrix;
};


#endif //LAB1_MATRIXSHORTESTPATHALGORITHM_H
