//
// Created by matnow on 10/14/18.
//

#ifndef LAB1_EDGELISTSHORTESTPATHALGORITHM_H
#define LAB1_EDGELISTSHORTESTPATHALGORITHM_H


#include <vector>
#include "ShortestPathAlgorithm.h"
#include "Edge.h"

class EdgeListShortestPathAlgorithm : public ShortestPathAlgorithm {

public:
    EdgeListShortestPathAlgorithm(int numberOfVertices, char *fileName);

    void compute(int *distanceArray, int *predecessorArray) override;

private:
    int numberOfVertices;
    std::vector<Edge> edges;

};


#endif //LAB1_EDGELISTSHORTESTPATHALGORITHM_H
