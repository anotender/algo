//
// Created by matnow on 10/11/18.
//

#include "Edge.h"
#include "Vertex.h"


Edge::Edge(Vertex &from, Vertex &to, int weight) : from(from), to(to), weight(weight) {}

Vertex &Edge::getFrom() const {
    return from;
}

Vertex &Edge::getTo() const {
    return to;
}

int Edge::getWeight() const {
    return weight;
}
