//
// Created by matnow on 10/11/18.
//

#ifndef LAB1_VERTEX_H
#define LAB1_VERTEX_H


class Vertex {
public:
    explicit Vertex(int number);

    int getNumber() const;

private:
    int number;
};


#endif //LAB1_VERTEX_H
