#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <values.h>
#include "Edge.h"
#include "ShortestPathAlgorithm.h"
#include "EdgeListShortestPathAlgorithm.h"
#include "MatrixShortestPathAlgorithm.h"
#include "CycleException.h"

using namespace std;

ShortestPathAlgorithm *getAlgorithm(char **argv) {
    if (string("edge-list") == argv[1]) {
        return new EdgeListShortestPathAlgorithm(atoi(argv[2]), argv[3]);
    } else if (string("matrix") == argv[1]) {
        return new MatrixShortestPathAlgorithm(atoi(argv[2]), argv[3]);
    }
    return nullptr;
}

int main(int argc, char **argv) {
    if (argc != 4) {
        std::cout << "Wrong number of parameters" << std::endl;
        return -1;
    }

    int numberOfVertices = atoi(argv[2]);
    int distanceArray[numberOfVertices];
    int predecessorArray[numberOfVertices];

    for (int j = 0; j < numberOfVertices; ++j) {
        distanceArray[j] = MAXINT;
        predecessorArray[j] = MAXINT;
    }
    distanceArray[0] = 0;
    predecessorArray[0] = 0;

    ShortestPathAlgorithm *algorithm = getAlgorithm(argv);

    if (algorithm == nullptr) {
        cout << "No algorithm found" << endl;
        return -1;
    }

    try {
        algorithm->compute(distanceArray, predecessorArray);

        for (int i = 0; i < numberOfVertices; ++i) {
            cout << "d[" << i << "] = " << distanceArray[i] << endl;
        }

        for (int i = 0; i < numberOfVertices; ++i) {
            cout << "p[" << i << "] = " << predecessorArray[i] << endl;
        }
    } catch (CycleException e) {
        cout << "Cycle" << endl;
    }

    return 0;
}
