//
// Created by matnow on 10/14/18.
//

#ifndef LAB1_SHORTESTPATHALGORITHM_H
#define LAB1_SHORTESTPATHALGORITHM_H


#include <string>

class ShortestPathAlgorithm {
public:
    virtual void compute(int *distanceArray, int *predecessorArray) = 0;
};


#endif //LAB1_SHORTESTPATHALGORITHM_H
