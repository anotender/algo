//
// Created by matnow on 10/14/18.
//

#include <values.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "EdgeListShortestPathAlgorithm.h"
#include "Vertex.h"
#include "Edge.h"
#include "CycleException.h"

EdgeListShortestPathAlgorithm::EdgeListShortestPathAlgorithm(int numberOfVertices, char *fileName) : numberOfVertices(
        numberOfVertices) {
    std::string line;
    std::ifstream myfile(fileName);
    if (!myfile.is_open()) {
        throw "Could not open the file";
    }
    while (getline(myfile, line)) {
        std::istringstream oss(line);
        std::string word;
        std::vector<int> values;
        while (getline(oss, word, ',')) {
            values.push_back(stoi(word));
        }
        auto *from = new Vertex(values[0]);
        auto *to = new Vertex(values[1]);
        Edge *edge = new Edge(*from, *to, values[2]);
        edges.push_back(*edge);
    }
    myfile.close();
}

void EdgeListShortestPathAlgorithm::compute(int *distanceArray, int *predecessorArray) {
    for (int i = 0; i < numberOfVertices; ++i) {
        for (auto &edge : edges) {
            int newWeight = distanceArray[edge.getFrom().getNumber()] + edge.getWeight();
            if (distanceArray[edge.getFrom().getNumber()] != MAXINT &&
                distanceArray[edge.getTo().getNumber()] > newWeight) {
                distanceArray[edge.getTo().getNumber()] = newWeight;
                predecessorArray[edge.getTo().getNumber()] = edge.getFrom().getNumber();
            }
        }
    }

    for (auto &edge : edges) {
        int newWeight = distanceArray[edge.getFrom().getNumber()] + edge.getWeight();
        if (distanceArray[edge.getFrom().getNumber()] != MAXINT &&
            distanceArray[edge.getTo().getNumber()] > newWeight) {
            throw *(new CycleException());
        }
    }
}
