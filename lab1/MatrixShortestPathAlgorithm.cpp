//
// Created by matnow on 10/14/18.
//

#include <sstream>
#include <vector>
#include <fstream>
#include <values.h>
#include <iostream>
#include "MatrixShortestPathAlgorithm.h"
#include "CycleException.h"

MatrixShortestPathAlgorithm::MatrixShortestPathAlgorithm(int numberOfVertices, char *fileName) : numberOfVertices(
        numberOfVertices) {
    this->incidenceMatrix = new int *[numberOfVertices];
    for (int i = 0; i < numberOfVertices; ++i) {
        this->incidenceMatrix[i] = new int[numberOfVertices];
        for (int j = 0; j < numberOfVertices; ++j) {
            this->incidenceMatrix[i][j] = MAXINT;
        }
    }
    std::string line;
    std::ifstream myfile(fileName);
    if (!myfile.is_open()) {
        throw "Could not open the file";
    }
    int i = 0;
    while (getline(myfile, line)) {
        std::istringstream oss(line);
        std::string word;
        int j = 0;
        while (getline(oss, word, ',')) {
            if (!word.empty()) {
                this->incidenceMatrix[i][j] = stoi(word);
            } else {
                this->incidenceMatrix[i][j] = MAXINT;
            }
            j++;
        }
        i++;
    }
    myfile.close();
}

void MatrixShortestPathAlgorithm::compute(int *distanceArray, int *predecessorArray) {
    for (int i = 0; i < numberOfVertices; ++i) {
        for (int u = 0; u < numberOfVertices; ++u) {
            for (int v = 0; v < numberOfVertices; ++v) {
                int newWeight = distanceArray[u] + incidenceMatrix[u][v];
                if (distanceArray[u] != MAXINT &&
                    incidenceMatrix[u][v] != MAXINT &&
                    distanceArray[v] > newWeight) {
                    distanceArray[v] = newWeight;
                    predecessorArray[v] = u;
                }
            }
        }
    }

    for (int u = 0; u < numberOfVertices; ++u) {
        for (int v = 0; v < numberOfVertices; ++v) {
            int newWeight = distanceArray[u] + incidenceMatrix[u][v];
            if (distanceArray[u] != MAXINT &&
                incidenceMatrix[u][v] != MAXINT &&
                distanceArray[v] > newWeight) {
                throw *(new CycleException());
            }
        }
    }
}
