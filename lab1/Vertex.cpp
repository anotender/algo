//
// Created by matnow on 10/11/18.
//

#include "Vertex.h"

Vertex::Vertex(int number) : number(number) {}

int Vertex::getNumber() const {
    return number;
}
