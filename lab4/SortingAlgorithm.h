//
// Created by matnow on 12/11/18.
//

#ifndef LAB4_SORTINGALGORITHM_H
#define LAB4_SORTINGALGORITHM_H

#include <vector>

using namespace std;

class SortingAlgorithm {
public:
    virtual void sort(vector<int> &) = 0;

protected:
    void merge(vector<int> &, vector<int> &, vector<int> &);
};


#endif //LAB4_SORTINGALGORITHM_H
