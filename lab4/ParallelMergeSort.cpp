//
// Created by matnow on 12/11/18.
//

#include <omp.h>
#include <iostream>
#include "ParallelMergeSort.h"

using namespace std;

ParallelMergeSort::ParallelMergeSort(SortingAlgorithm *serialMergeSort) : serialMergeSort(serialMergeSort) {}

int ParallelMergeSort::omp_thread_count() {
    int n = 0;
#pragma omp parallel reduction(+:n)
    n += 1;
    return n;
}

void ParallelMergeSort::sort(vector<int> &array) {
    int thread_count = omp_thread_count();
    unsigned long partLength = array.size() / thread_count;
    vector<vector<int>> partialResults(thread_count);

#pragma omp parallel
    {
        int thread_num = omp_get_thread_num();
        unsigned long partStart = thread_num * partLength;
        unsigned long partEnd = partStart + partLength;
        if (thread_num + 1 == thread_count) {
            partEnd = array.size();
        }

        vector<int> part(array.begin() + partStart, array.begin() + partEnd);
        serialMergeSort->sort(part);
        partialResults[thread_num] = part;
    };

    while (partialResults.size() != 1) {
        vector<int> mergeResult;
        merge(mergeResult, partialResults[0], partialResults[1]);
        partialResults.erase(partialResults.begin());
        partialResults.erase(partialResults.begin());
        partialResults.push_back(mergeResult);
    }
    array = partialResults[0];
}
