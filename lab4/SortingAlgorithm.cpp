//
// Created by matnow on 12/11/18.
//

#include "SortingAlgorithm.h"

void SortingAlgorithm::merge(vector<int> &array, vector<int> &array1, vector<int> &array2) {
    array.clear();

    int i, j;
    for (i = 0, j = 0; i < array1.size() && j < array2.size();) {
        if (array1[i] <= array2[j]) {
            array.push_back(array1[i]);
            i++;
        } else if (array1[i] > array2[j]) {
            array.push_back(array2[j]);
            j++;
        }
    }

    while (i < array1.size()) {
        array.push_back(array1[i]);
        i++;
    }

    while (j < array2.size()) {
        array.push_back(array2[j]);
        j++;
    }
}
