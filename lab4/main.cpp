#include <iostream>
#include <omp.h>
#include "SortingAlgorithm.h"
#include "SerialMergeSort.h"
#include "ParallelMergeSort.h"

#define N 10

using namespace std;

void print_vector(vector<int> &v) {
    for (int i : v) {
        cout << i << " ";
    }
    cout << endl;
}

int main() {
    srand(time(NULL));;

    vector<int> numbers1;
    vector<int> numbers2;
    for (int i = 0; i < N; ++i) {
        int number = rand() % 100;
        numbers1.push_back(number);
        numbers2.push_back(number);
    }
    print_vector(numbers1);

    double start_time = omp_get_wtime();
    SortingAlgorithm *serialMergeSort = new SerialMergeSort();
    serialMergeSort->sort(numbers1);
    double end_time = omp_get_wtime();

    cout << "Serial merge sort time: " << end_time - start_time << " [s]" << endl;
    print_vector(numbers1);

    start_time = omp_get_wtime();
    SortingAlgorithm *parallelMergeSort = new ParallelMergeSort(serialMergeSort);
    parallelMergeSort->sort(numbers2);
    end_time = omp_get_wtime();

    cout << "Parallel merge sort time: " << end_time - start_time << " [s]" << endl;
    print_vector(numbers2);

    return 0;
}