//
// Created by matnow on 12/11/18.
//

#ifndef LAB4_SERIALMERGESORT_H
#define LAB4_SERIALMERGESORT_H


#include "SortingAlgorithm.h"

class SerialMergeSort : public SortingAlgorithm {
public:
    void sort(vector<int> &) override;
};


#endif //LAB4_SERIALMERGESORT_H
