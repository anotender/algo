//
// Created by matnow on 12/11/18.
//

#ifndef LAB4_PARALLELMERGESORT_H
#define LAB4_PARALLELMERGESORT_H


#include "SortingAlgorithm.h"
#include "SerialMergeSort.h"

class ParallelMergeSort : public SortingAlgorithm {
public:
    explicit ParallelMergeSort(SortingAlgorithm *serialMergeSort);

    void sort(vector<int> &) override;

private:
    SortingAlgorithm *serialMergeSort;

    int omp_thread_count();
};


#endif //LAB4_PARALLELMERGESORT_H
