//
// Created by matnow on 12/11/18.
//

#include "SerialMergeSort.h"

void SerialMergeSort::sort(vector<int> &array) {
    if (1 < array.size()) {
        vector<int> array1(array.begin(), array.begin() + array.size() / 2);
        vector<int> array2(array.begin() + array.size() / 2, array.end());

        sort(array1);
        sort(array2);

        merge(array, array1, array2);
    }
}
