//
// Created by matnow on 11/10/18.
//

#include <iostream>
#include <queue>
#include <values.h>
#include "MatrixMaxFlowAlgorithm.h"

MatrixMaxFlowAlgorithm::MatrixMaxFlowAlgorithm(int numberOfVertices, char *fileName) {
    this->numberOfVertices = numberOfVertices;
    this->capacity = readFile(fileName);
}

int MatrixMaxFlowAlgorithm::compute() {
    int flow[numberOfVertices][numberOfVertices];
    for (int u = 0; u < numberOfVertices; ++u) {
        for (int v = 0; v < numberOfVertices; ++v) {
            flow[u][v] = 0;
        }
    }

    int maxFlow = 0;
    int residualCapacities[numberOfVertices];

    while (true) {
        int predecessors[numberOfVertices];
        predecessors[0] = -2;
        for (int i = 1; i < numberOfVertices; ++i) {
            predecessors[i] = -1;
        }

        queue<int> q;
        q.push(0);

        residualCapacities[0] = MAXINT;

        bool augmentingPathFound = false;

        while (!q.empty() && !augmentingPathFound) {
            int u = q.front();
            q.pop();

            for (int v = 0; v < numberOfVertices; ++v) {
                int residualCapacity = capacity[u][v] - flow[u][v];
                if (residualCapacity != 0 && predecessors[v] == -1) {
                    predecessors[v] = u;
                    residualCapacities[v] = min(residualCapacities[u], residualCapacity);
                    if (v == numberOfVertices - 1) {
                        maxFlow += residualCapacities[v];
                        int i = v;
                        while (i != 0) {
                            u = predecessors[i];
                            flow[u][i] += residualCapacities[v];
                            flow[i][u] -= residualCapacities[v];
                            i = u;
                        }
                        augmentingPathFound = true;
                    }
                    q.push(v);
                }
            }
        }

        if (!augmentingPathFound) { break; }
    }

    return maxFlow;
}
