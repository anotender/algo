#include <iostream>
#include "MaxFlowAlgorithm.h"
#include "EdgeListMaxFlowAlgorithm.h"
#include "MatrixMaxFlowAlgorithm.h"

MaxFlowAlgorithm *getMaxFlowAlgorithm(char *type, int numberOfVertices, char *fileName) {
    if (string("edge-list") == type) {
        return new EdgeListMaxFlowAlgorithm(numberOfVertices, fileName);
    } else if (string("matrix") == type) {
        return new MatrixMaxFlowAlgorithm(numberOfVertices, fileName);
    }
    return nullptr;
}

int main(int argc, char **argv) {
    if (argc != 4) {
        cout << "Wrong number of arguments" << endl;
        return -1;
    }

    MaxFlowAlgorithm *algorithm = getMaxFlowAlgorithm(argv[1], atoi(argv[2]), argv[3]);
    cout << "Max flow = " << algorithm->compute() << endl;
    return 0;
}