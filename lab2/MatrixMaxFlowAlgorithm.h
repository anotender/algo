//
// Created by matnow on 11/10/18.
//

#ifndef LAB2_MATRIXMAXFLOWALGORITHM_H
#define LAB2_MATRIXMAXFLOWALGORITHM_H


#include "MaxFlowAlgorithm.h"

class MatrixMaxFlowAlgorithm : public MaxFlowAlgorithm {
public:
    MatrixMaxFlowAlgorithm(int numberOfVertices, char *fileName);

    int compute() override;

private:
    int numberOfVertices;
    vector<vector<int>> capacity;
};


#endif //LAB2_MATRIXMAXFLOWALGORITHM_H
