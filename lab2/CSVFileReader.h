//
// Created by matnow on 11/2/18.
//

#ifndef LAB2_CSVFILEREADER_H
#define LAB2_CSVFILEREADER_H

#include <string>
#include <vector>

using namespace std;

class CSVFileReader {
public:
    vector<vector<int>> read(string filename);
};


#endif //LAB2_CSVFILEREADER_H
