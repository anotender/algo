//
// Created by matnow on 11/10/18.
//

#include <iostream>
#include <queue>
#include <values.h>
#include <list>
#include "EdgeListMaxFlowAlgorithm.h"

EdgeListMaxFlowAlgorithm::EdgeListMaxFlowAlgorithm(int numberOfVertices, char *fileName) {
    this->numberOfVertices = numberOfVertices;
    this->edges = readFile(fileName);
}

int EdgeListMaxFlowAlgorithm::compute() {
    int flow[numberOfVertices][numberOfVertices];
    int capacity[numberOfVertices][numberOfVertices];
    for (int u = 0; u < numberOfVertices; ++u) {
        for (int v = 0; v < numberOfVertices; ++v) {
            flow[u][v] = capacity[u][v] = 0;
        }
    }

    list<int> neighbours[numberOfVertices];
    for (auto &edge : this->edges) {
        capacity[edge[0]][edge[1]] = edge[2];
        neighbours[edge[0]].push_back(edge[1]);
    }

    for (int u = 0; u < numberOfVertices; ++u) {
        for (auto &uNeighbour : neighbours[u]) {
            bool hasBackwardLink = false;
            for (auto &vNeighbour : neighbours[uNeighbour]) {
                if (vNeighbour == uNeighbour) {
                    hasBackwardLink = true;
                    break;
                }
            }

            if (!hasBackwardLink) {
                neighbours[uNeighbour].push_back(u);
            }
        }
    }

    int maxFlow = 0;
    int residualCapacities[numberOfVertices];

    while (true) {
        int predecessors[numberOfVertices];
        for (int i = 1; i < numberOfVertices; ++i) {
            predecessors[i] = -1;
        }
        predecessors[0] = -2;

        queue<int> q;
        q.push(0);

        residualCapacities[0] = MAXINT;

        bool augmentingPathFound = false;

        while (!q.empty() && !augmentingPathFound) {
            int u = q.front();
            q.pop();

            for (auto &uNeighbour : neighbours[u]) {
                int residualCapacity = capacity[u][uNeighbour] - flow[u][uNeighbour];
                if (residualCapacity != 0 && predecessors[uNeighbour] == -1) {
                    predecessors[uNeighbour] = u;
                    residualCapacities[uNeighbour] = min(residualCapacities[u], residualCapacity);
                    if (uNeighbour == numberOfVertices - 1) {
                        augmentingPathFound = true;
                        break;
                    } else {
                        q.push(uNeighbour);
                    }
                }
            }
        }

        if (!augmentingPathFound) { break; }

        maxFlow += residualCapacities[numberOfVertices - 1];

        int u;
        for (int v = numberOfVertices - 1; v != 0; v = u) {
            u = predecessors[v];
            for (auto &uNeighbour : neighbours[u]) {
                if (uNeighbour == v) {
                    flow[u][v] += residualCapacities[numberOfVertices - 1];
                    break;
                }
            }
            for (auto &vNeighbour : neighbours[v]) {
                if (vNeighbour == u) {
                    flow[v][u] -= residualCapacities[numberOfVertices - 1];
                    break;
                }
            }
        }
    }

    return maxFlow;
}
