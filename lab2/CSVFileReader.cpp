//
// Created by matnow on 11/2/18.
//

#include <fstream>
#include <sstream>
#include "CSVFileReader.h"

using namespace std;

vector<vector<int>> CSVFileReader::read(string fileName) {
    string line;
    ifstream myfile(fileName);
    if (!myfile.is_open()) {
        throw "Could not open the file";
    }

    vector<std::vector<int>> vectors;
    while (getline(myfile, line)) {
        istringstream oss(line);
        string word;
        vector<int> row;
        while (getline(oss, word, ',')) {
            row.push_back(stoi(word));
        }
        vectors.push_back(row);
    }
    myfile.close();

    return vectors;
}
