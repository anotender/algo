//
// Created by matnow on 11/10/18.
//

#ifndef LAB2_EDGELISTMAXFLOWALGORITHM_H
#define LAB2_EDGELISTMAXFLOWALGORITHM_H


#include "MaxFlowAlgorithm.h"

class EdgeListMaxFlowAlgorithm : public MaxFlowAlgorithm {
public:
    EdgeListMaxFlowAlgorithm(int numberOfVertices, char *fileName);

    int compute() override;

private:
    int numberOfVertices;
    vector<vector<int>> edges;
};


#endif //LAB2_EDGELISTMAXFLOWALGORITHM_H
