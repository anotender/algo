//
// Created by matnow on 11/10/18.
//

#ifndef LAB2_MAXFLOWALGORITHM_H
#define LAB2_MAXFLOWALGORITHM_H


#include "CSVFileReader.h"

class MaxFlowAlgorithm {
public:
    virtual int compute() = 0;

protected:
    vector<vector<int>> readFile(char *fileName);

private:
    CSVFileReader csvFileReader;
};


#endif //LAB2_MAXFLOWALGORITHM_H
