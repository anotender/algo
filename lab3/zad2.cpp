#include <iostream>
#include <omp.h>
#include <cmath>
#include "zad2.h"


#define R 10000
#define N R*R

using namespace std;

int get_serial_random_value() {
    int random_number = rand() % (R + 1);
    if (rand() % 2) {
        return random_number;
    } else {
        return (-1) * random_number;
    }
}

int get_parallel_random_value(unsigned int *seed) {
    int random_number = rand_r(seed) % (R + 1);
    if (rand_r(seed) % 2) {
        return random_number;
    } else {
        return (-1) * random_number;
    }
}

void serial_zad2() {
    srand(time(NULL));
    double start_time = omp_get_wtime();
    int in_circle_count = 0;
    for (int i = 0; i < N; ++i) {
        int x = get_serial_random_value();
        int y = get_serial_random_value();

        if (x * x + y * y <= R * R) {
            ++in_circle_count;
        }
    }
    double end_time = omp_get_wtime();

    double pi = (in_circle_count * 4) / ((double) N);
    cout << "Serial zad2 result = " << pi << endl;
    cout << "Time = " << end_time - start_time << endl << endl;
}

void parallel_chunks_zad2() {
    int in_circle_count = 0;
    double start_time = omp_get_wtime();
#pragma omp parallel
    {
        srand(time(NULL));
        auto thread_num = (unsigned int) omp_get_thread_num();
        int thread_in_circle_count = 0;
        int num_threads = omp_get_num_threads();
        for (int i = 0; i < N / num_threads; ++i) {
            int x = get_parallel_random_value(&thread_num);
            int y = get_parallel_random_value(&thread_num);

            if (x * x + y * y <= R * R) {
                ++thread_in_circle_count;
            }
        }
        in_circle_count += thread_in_circle_count;
    };
    double end_time = omp_get_wtime();

    double pi = (in_circle_count * 4) / ((double) N);
    cout << "Parallel chunks zad2 result = " << pi << endl;
    cout << "Time = " << end_time - start_time << endl << endl;
}

void parallel_for_reduction_zad2() {
    double start_time = omp_get_wtime();
    int in_circle_count = 0;
    int n = N;
#pragma omp parallel
    {
        srand(time(NULL));
        auto thread_num = (unsigned int) omp_get_thread_num();
#pragma omp for reduction(+:in_circle_count)
        for (int i = 0; i < n; ++i) {
            int x = get_parallel_random_value(&thread_num);
            int y = get_parallel_random_value(&thread_num);

            if (x * x + y * y <= R * R) {
                in_circle_count = in_circle_count + 1;
            }
        }
    };
    double end_time = omp_get_wtime();

    double pi = (in_circle_count * 4) / ((double) N);
    cout << "Parallel for reduction zad2 result = " << pi << endl;
    cout << "Time = " << end_time - start_time << endl << endl;
}
