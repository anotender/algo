cmake_minimum_required(VERSION 3.12)
project(lab3)

# added -fopenmp
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -fopenmp")

set(SOURCE_FILES zad1.cpp zad2.cpp main.cpp zad1.h zad2.h)
add_executable(lab3 ${SOURCE_FILES})