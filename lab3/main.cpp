#include <cstdio>
#include <time.h>
#include <cstdlib>
#include "zad1.h"
#include "zad2.h"

int main() {

    serial_zad1();
    parallel_chunks_zad1();
    parallel_for_reduction_zad1();

    serial_zad2();
    parallel_chunks_zad2();
    parallel_for_reduction_zad2();

    return 0;
}