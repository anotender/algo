//
// Created by matnow on 11/25/18.
//

#ifndef LAB3_ZAD1_H
#define LAB3_ZAD1_H

void serial_zad1();

void parallel_chunks_zad1();

void parallel_for_reduction_zad1();

#endif //LAB3_ZAD1_H
