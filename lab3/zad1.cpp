#include <iostream>
#include <omp.h>

#define DX 0.00000001
#define N 1/DX

using namespace std;

double f(double x) {
    return 1 / (1 + x * x);
}

void serial_zad1() {
    double start_time = omp_get_wtime();
    double result = 0;
    for (int i = 0; i < N; ++i) {
        result += f(i * DX) * DX;
    }
    double end_time = omp_get_wtime();

    cout << "Serial zad1 result = " << 4 * result << endl;
    cout << "Time = " << end_time - start_time << endl << endl;
}

void parallel_chunks_zad1() {
    int num_threads = omp_get_num_threads();
    double partial_results[num_threads];
    for (int i = 0; i < num_threads; ++i) {
        partial_results[i] = 0;
    }

    double result = 0;
    double start_time = omp_get_wtime();
#pragma omp parallel
    {
        int thread_num = omp_get_thread_num();
        int num_threads = omp_get_num_threads();

        double partial_result = 0;
        for (int i = thread_num; i < N; i = i + num_threads) {
            partial_result += f(i * DX) * DX;
        }
        result += partial_result;
    };
    double end_time = omp_get_wtime();

    cout << "Parallel chunks zad1 result = " << 4 * result << endl;
    cout << "Time = " << end_time - start_time << endl << endl;
}

void parallel_for_reduction_zad1() {
    double start_time = omp_get_wtime();
    double result = 0;
    int n = N;
#pragma omp parallel for reduction(+:result)
    for (int i = 0; i < n; ++i) {
        result += f(i * DX) * DX;
    }
    double end_time = omp_get_wtime();

    cout << "Parallel for reduction zad1 result = " << 4 * result << endl;
    cout << "Time = " << end_time - start_time << endl << endl;
}