//
// Created by matnow on 11/25/18.
//

#ifndef LAB3_ZAD2_H
#define LAB3_ZAD2_H

void serial_zad2();

void parallel_chunks_zad2();

void parallel_for_reduction_zad2();

#endif //LAB3_ZAD2_H
