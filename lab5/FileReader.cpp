//
// Created by matnow on 12/22/18.
//

#include <fstream>
#include <iostream>
#include "FileReader.h"

vector<string> FileReader::read(string filename) {
    vector<string> lines;
    ifstream inputFile(filename);

    for (string line; getline(inputFile, line);) {
        lines.push_back(line);
    }
    inputFile.close();

    return lines;
}
