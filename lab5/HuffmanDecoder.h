//
// Created by matnow on 12/24/18.
//

#ifndef LAB5_HUFFMANDECODER_H
#define LAB5_HUFFMANDECODER_H


#include "FileReader.h"
#include "FileWriter.h"
#include "Node.h"
#include <map>

class HuffmanDecoder {
public:
    HuffmanDecoder(FileReader *reader, FileWriter *writer);

    void decode(string, string, string);

private:
    FileReader *reader;
    FileWriter *writer;

    Node *initialize_huffman_tree(map<char, string> &);
};


#endif //LAB5_HUFFMANDECODER_H
