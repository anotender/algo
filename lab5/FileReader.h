//
// Created by matnow on 12/22/18.
//

#ifndef LAB5_FILEREADER_H
#define LAB5_FILEREADER_H

#include <string>
#include <vector>

using namespace std;

class FileReader {
public:
    vector <string> read(string);
};


#endif //LAB5_FILEREADER_H
