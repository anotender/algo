#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include "HuffmanEncoder.h"
#include "HuffmanDecoder.h"

using namespace std;


int main() {
    FileReader reader;
    FileWriter writer;
    HuffmanEncoder encoder(&reader, &writer);
    HuffmanDecoder decoder(&reader, &writer);

    string input = "input.txt";
    string output = "output.txt";
    string decoded_output = "decoded_output.txt";
    encoder.encode(input, output);

    decoder.decode(output, decoded_output, "codes.txt");

    return 0;
}