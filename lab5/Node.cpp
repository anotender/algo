//
// Created by matnow on 12/24/18.
//

#include "Node.h"

Node::Node(char c, int numberOfOccurrences, Node *left, Node *right) : c(c), numberOfOccurrences(numberOfOccurrences),
                                                                       left(left), right(right) {}

void Node::set_c(char c) {
    Node::c = c;
}

char Node::get_c() const {
    return c;
}

int Node::get_number_of_occurrences() const {
    return numberOfOccurrences;
}

void Node::set_left(Node *left) {
    Node::left = left;
}

Node *Node::get_left() const {
    return left;
}

bool Node::has_left() {
    return left != nullptr;
}

Node *Node::get_right() const {
    return right;
}

void Node::set_right(Node *right) {
    Node::right = right;
}

bool Node::has_right() {
    return right != nullptr;
}
