//
// Created by matnow on 12/20/18.
//

#ifndef LAB5_HUFFMANENCODER_H
#define LAB5_HUFFMANENCODER_H

#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <queue>
#include "FileReader.h"
#include "FileWriter.h"
#include "Node.h"
#include "NodeComparator.h"

using namespace std;

class HuffmanEncoder {
public:
    void encode(string &, string &);

    HuffmanEncoder(FileReader *reader, FileWriter *writer);

private:
    FileReader *reader;

    FileWriter *writer;

    Node *generate_huffman_tree(priority_queue<Node *, vector<Node *>, NodeComparator>);

    void initialize_codes_map(Node *, string, map<char, string> &);

    void initialize_nodes(vector<string> &, priority_queue<Node *, vector<Node *>, NodeComparator> &);

    void save_codes_map(map<char, string> &);
};

#endif //LAB5_HUFFMANENCODER_H
