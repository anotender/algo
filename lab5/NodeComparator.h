//
// Created by matnow on 12/24/18.
//

#ifndef LAB5_NODECOMPARATOR_H
#define LAB5_NODECOMPARATOR_H


#include "Node.h"

class NodeComparator {
public:
    bool operator()(Node *, Node *);
};


#endif //LAB5_NODECOMPARATOR_H
