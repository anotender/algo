//
// Created by matnow on 12/23/18.
//

#ifndef LAB5_FILEWRITER_H
#define LAB5_FILEWRITER_H

#include <vector>
#include <string>

using namespace std;

class FileWriter {
public:
    void write(string, vector<string>);

};


#endif //LAB5_FILEWRITER_H
