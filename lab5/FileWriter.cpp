//
// Created by matnow on 12/23/18.
//

#include <fstream>
#include "FileWriter.h"

void FileWriter::write(string filename, vector<string> lines) {
    ofstream file;
    file.open(filename);

    for (const auto &line : lines) {
        file << line << endl;
    }

    file.close();
}
