//
// Created by matnow on 12/24/18.
//

#include <iostream>
#include <sstream>
#include "HuffmanDecoder.h"
#include "Node.h"

HuffmanDecoder::HuffmanDecoder(FileReader *reader, FileWriter *writer) : reader(reader), writer(writer) {}

void HuffmanDecoder::decode(string input_path, string output_path, string codes_path) {
    vector<string> codeLines = reader->read(codes_path);
    vector<string> inputLines = reader->read(input_path);

    map<char, string> codes_map;
    for (string &codeLine:codeLines) {
        string c = codeLine.substr(0, 1);
        string code = codeLine.substr(2);
        codes_map[c.front()] = code;
    }

    Node *root = initialize_huffman_tree(codes_map);
    vector<string> decodedLines;
    for (string &line:inputLines) {
        if (line.empty()) {
            decodedLines.push_back("");
            continue;
        }

        string decodedLine;
        Node *it = root;
        for (char &c : line) {
            if (!it->get_left() && !it->has_right()) {
                decodedLine += it->get_c();
                it = root;
            }
            if (c == '0' && it->has_left()) {
                it = it->get_left();
            } else if (c == '1' && it->has_right()) {
                it = it->get_right();
            }
        }
        decodedLine += it->get_c();
        decodedLines.push_back(decodedLine);
    }

    writer->write(output_path, decodedLines);
}

Node *HuffmanDecoder::initialize_huffman_tree(map<char, string> &codes_map) {
    Node *root = new Node();
    for (auto const &pair : codes_map) {
        char character = pair.first;
        string code = pair.second;

        Node *it = root;
        for (char &c : code) {
            if (c == '0') {
                if (!it->has_left()) {
                    it->set_left(new Node());
                }
                it = it->get_left();
            } else {
                if (!it->has_right()) {
                    it->set_right(new Node());
                }
                it = it->get_right();
            }
        }
        it->set_c(character);
    }
    return root;
}
