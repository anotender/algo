//
// Created by matnow on 12/24/18.
//

#include "NodeComparator.h"

bool NodeComparator::operator()(Node *n1, Node *n2) {
    return n1->get_number_of_occurrences() > n2->get_number_of_occurrences();
}
