//
// Created by matnow on 12/20/18.
//

#include "HuffmanEncoder.h"

Node *HuffmanEncoder::generate_huffman_tree(priority_queue<Node *, vector<Node *>, NodeComparator> nodes) {
    while (nodes.size() != 1) {
        Node *n1 = nodes.top();
        nodes.pop();

        Node *n2 = nodes.top();
        nodes.pop();

        Node *n3 = new Node(0, n1->get_number_of_occurrences() + n2->get_number_of_occurrences(), n1, n2);
        nodes.push(n3);
    }
    return nodes.top();
}

void
HuffmanEncoder::initialize_nodes(vector<string> &lines, priority_queue<Node *, vector<Node *>, NodeComparator> &nodes) {
    map<char, int> frequencies;
    for (string &line:lines) {
        for (char &c:line) {
            frequencies[c]++;
        }
    }

    for (auto const &f : frequencies) {
        nodes.push(new Node(f.first, f.second));
    }
}

void HuffmanEncoder::initialize_codes_map(Node *n, string currentCode, map<char, string> &codesMap) {
    if (!n->has_left() && !n->has_right()) {
        codesMap[n->get_c()] = currentCode;
        return;
    }
    initialize_codes_map(n->get_left(), currentCode + "0", codesMap);
    initialize_codes_map(n->get_right(), currentCode + "1", codesMap);
}

void HuffmanEncoder::encode(string &input, string &output) {
    vector<string> lines = reader->read(input);

    priority_queue<Node *, vector<Node *>, NodeComparator> nodes;
    initialize_nodes(lines, nodes);

    Node *root = generate_huffman_tree(nodes);

    map<char, string> codesMap;
    initialize_codes_map(root, "", codesMap);
    save_codes_map(codesMap);

    vector<string> encodedLines;
    for (string &line:lines) {
        string encoded_line;
        for (char &c:line) {
            encoded_line += codesMap[c];
        }
        encodedLines.push_back(encoded_line);
    }

    writer->write(output, encodedLines);
}

void HuffmanEncoder::save_codes_map(map<char, string> &codesMap) {
    vector<string> codeLines;
    codeLines.reserve(codesMap.size());
    for (auto const &f : codesMap) {
        codeLines.push_back(f.first + string("=") + f.second);
    }
    writer->write("codes.txt", codeLines);
}

HuffmanEncoder::HuffmanEncoder(FileReader *reader, FileWriter *writer) : reader(reader), writer(writer) {}
