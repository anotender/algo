//
// Created by matnow on 12/24/18.
//

#ifndef LAB5_NODE_H
#define LAB5_NODE_H


class Node {
public:
    explicit Node(char = 0, int = 0, Node * = nullptr, Node * = nullptr);

    void set_c(char c);

    char get_c() const;

    int get_number_of_occurrences() const;

    Node *get_left() const;

    void set_left(Node *left);

    bool has_left();

    Node *get_right() const;

    void set_right(Node *right);

    bool has_right();

private:
    char c;
    int numberOfOccurrences;
    Node *left;
    Node *right;
};


#endif //LAB5_NODE_H
